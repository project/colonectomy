<?php

/**
 * @file
 * Allows colons to be removed from field labels.
 */

/**
 * Implements hook_field_formatter_info_alter().
 */
function colonectomy_field_formatter_info_alter(&$info) {
  foreach ($info as $instance => $value) {
    $info[$instance]['settings'] += array(
      'colonectomy' => FALSE,
    );
  }
}

/**
 * Implements hook_field_formatter_settings_summary_alter().
 */
function colonectomy_field_formatter_settings_summary_alter(&$summary, $context) {
  $display = $context['instance']['display'][$context['view_mode']];
  $settings = $display['settings'];

  if (!empty($settings['colonectomy'])) {
    if (!empty($summary)) {
      $summary .= '<br />';
    }
    $summary .= t('No colon on the label');
  }
}

function colonectomy_field_formatter_settings_form_alter(&$settings_form, $context) {
  $display = $context['instance']['display'][$context['view_mode']];
  $settings = $display['settings'];

  $settings_form['colonectomy'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove colon from field label'),
    '#default_value' => $settings['colonectomy'],
  );
}

/**
 * Theme registry override for the theme_form_element() function
 */
function colonectomy_theme_registry_alter(&$theme_registry) {
  if (!empty($theme_registry['field'])) {
    $theme_registry['field']['function'] = 'colonectomy_field';
  }
}

/**
 * Implements hook_preprocess_field().
 *
 * Adds a custom field class to the field's classes_array
 * according to the field formatter settings. 
 */
function colonectomy_preprocess_field(&$variables, $hook) {
  $field_name   = $variables['element']['#field_name'];
  $entity_type  = $variables['element']['#entity_type'];
  $bundle       = $variables['element']['#bundle'];
  $view_mode      = $variables['element']['#view_mode'];

  $formatter_info = field_formatter_settings_get_instance_display_settings($entity_type, $field_name, $bundle, $view_mode);
  // Output label with the colon (and &nbsp; character) or not.
  if (!$variables['label_hidden'] && empty($formatter_info['colonectomy'])) {
    $variables['label'] .= ':&nbsp;';
  }
}

/**
 * Overridden theme_field().
 */
function colonectomy_field(&$variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . '</div>';
  }

  // Render the items.
  $output .= '<div class="field-items"' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  $output .= '</div>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';
  return $output;
}
